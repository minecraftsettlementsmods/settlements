#!/usr/bin/env bash

cd ./settlements-core
mkdir ./run
cp -r ./test-int/int-world/** ./run
bash ./gradlew npmInstall
bash ./gradlew runServer > server.log &
bash ./gradlew runIntTest
kill $!