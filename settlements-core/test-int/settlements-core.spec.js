const assert = require('chai').assert;
const expect = require('chai').expect;
const should = require('chai').should();
const mineflayer = require('mineflayer');
const forgeHandshake = require('minecraft-protocol-forge').forgeHandshake;

describe('Mineflayer', function() {
    this.timeout(10000);
    var bot;
    beforeEach(function(done) {
        bot = mineflayer.createBot({
          host: "localhost",
        });
        forgeHandshake(bot._client, {
        forgeMods: [
          { modid: 'minecraft', version: '1.12.2' },
          { modid: 'mcp', version: '9.42' },
          { modid: 'FML', version: '8.0.99.99' },
          { modid: 'forge', version: '14.23.5.2768' },
          { modid: 'settlements_core', version: '0.0.1' }
        ]});
        bot.on('spawn', function() {
            done();
        });
    });

    afterEach(function() {
        bot.end();
    });

    describe('chat', function() {
        it('should find own chat message', function(done) {
            const MESSAGE = 'a unique chat message';
            bot.on('chat', function(username, message) {
                if (username === bot.username) {
                    message.should.be.a('string');
                    message.should.equal(MESSAGE);
                    done();
                }
            });
            bot.chat(MESSAGE);
        });
    });

    describe('chat2', function() {
        it('should find own chat message', function(done) {
            const MESSAGE = 'a unique chat message 2';
            bot.on('chat', function(username, message) {
                if (username === bot.username) {
                    message.should.be.a('string');
                    message.should.equal(MESSAGE);
                    done();
                }
            });
            bot.chat(MESSAGE);
        });
    });
});
