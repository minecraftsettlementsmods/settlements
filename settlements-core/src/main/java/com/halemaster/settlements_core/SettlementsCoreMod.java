/* Licensed under MIT */
package com.halemaster.settlements_core;

import net.minecraft.init.Blocks;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(
  modid = SettlementsCoreMod.MODID,
  name = SettlementsCoreMod.NAME,
  version = SettlementsCoreMod.VERSION
)
public class SettlementsCoreMod {
  public static final String MODID = "settlements_core";
  public static final String NAME = "Settlements Core";
  public static final String VERSION = "0.0.1";

  private static Logger logger;

  @EventHandler
  public void preInit(FMLPreInitializationEvent event) {
    logger = event.getModLog();
  }

  @EventHandler
  public void init(FMLInitializationEvent event) {
    // some example code
    logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
  }
}
